/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SegundaEvaluacion.Practica2;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.Random;

/**
 *
 * @author Jordi
 */
public class ScheduledThreadPool {

    private static String[] displays;

    public static void main(String[] args) throws InterruptedException {

        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(1);

        scheduledPool.scheduleWithFixedDelay((Runnable) scheduledPool, 0, 5, TimeUnit.SECONDS);

        Thread.sleep(60000);
        scheduledPool.shutdown();

        ExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    }
}
