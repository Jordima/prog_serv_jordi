package SegundaEvaluacion.Practica6.ChatServidor;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author Jordi
 */

public class Servidor {

    public static void main(String[] args) {
        Servidor servidor = new Servidor();
        servidor.escuchar();
    }
    //puerto y nº de conexiones
    private final int puerto = 1234;
    private final int noConexiones = 20;
    //lista para los sockets
    private LinkedList<Socket> usuarios = new LinkedList<Socket>();

    //Funcion para que el servidor empieze a recibir conexiones de clientes !!!
    public void escuchar() {
        try {
            ServerSocket servidor = new ServerSocket(puerto, noConexiones);
            //ciclo infinito para escuchar nuevos clientes
            while (true) {
                System.out.println("Escuchando....");
                Socket cliente = servidor.accept();
                usuarios.add(cliente);
                Runnable run = new HiloServidor(cliente, usuarios);
                Thread hilo = new Thread(run);
                hilo.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
