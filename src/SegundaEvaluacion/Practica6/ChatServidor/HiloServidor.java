package SegundaEvaluacion.Practica6.ChatServidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author Jordi
 */
public class HiloServidor implements Runnable {

    //variables para el hilo
    private Socket socket;
    private DataInputStream input;
    private DataOutputStream output;
    private LinkedList<Socket> usuarios = new LinkedList<Socket>();

    public HiloServidor(Socket soc, LinkedList users) {
        socket = soc;
        usuarios = users;
    }

    @Override
    public void run() {
        try {
            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());
            output.writeUTF("<h2>Bienvenido....</h2>");
            //Ciclo infinito para escuchar por mensajes del cliente !!! importante
            while (true) {
                String recibido = input.readUTF();
                //Cuando se recibe un mensaje se envia a todos los usuarios conectados 
                for (int i = 0; i < usuarios.size(); i++) {
                    output = new DataOutputStream(usuarios.get(i).getOutputStream());
                    output.writeUTF(recibido);
                }
            }
        } catch (IOException e) {
            //controlar la excepcion por si el cliente se desconecta
            for (int i = 0; i < usuarios.size(); i++) {
                if (usuarios.get(i) == socket) {
                    usuarios.remove(i);
                    break;
                }
            }
        }
    }
}
