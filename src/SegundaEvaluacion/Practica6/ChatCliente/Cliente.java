package SegundaEvaluacion.Practica6.ChatCliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.DefaultCaret;

/**
 *
 * @author Jordi
 */
public class Cliente implements Runnable {

    //Variables para la conexion y comunicacion
    private Socket cliente;
    private DataInputStream in;
    private DataOutputStream out;
    private int puerto = 2027;//puerto mismo que servidor
    private String host = "localhost";//Localhost en esye caso, cambiar para comunicarse con otro.
    private String mensajes = "";
    JEditorPane panel;

    public Cliente(JEditorPane panel) {
        this.panel = panel;
        try {
            cliente = new Socket(host, puerto);
            in = new DataInputStream(cliente.getInputStream());
            out = new DataOutputStream(cliente.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            //Ciclo para mostrar mensajes por el panel.
            while (true) {
                mensajes += in.readUTF();
                panel.setText(mensajes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //enviar mensajes
    public void enviarMsg(String msg) {
        try {
            out.writeUTF(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
