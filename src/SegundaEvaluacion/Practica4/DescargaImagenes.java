/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SegundaEvaluacion.Practica4;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Jordi
 */
public class DescargaImagenes {

    //URL de la web
    private static final String webURL = "http://jordima.blogspot.es";
    //Ruta de la carpeta
    private static final String rutaCarpeta = "target";

    public static void main(String[] args) throws InterruptedException {
        try {
            //Conectar a la web y coger el HTML
            Document doc = Jsoup.connect(webURL).get();

            //Coger los elementros con IMG
            Elements img = doc.getElementsByTag("img");

            for (Element el : img) {

                //Por cada elemento obtiene la url del src
                String src = el.absUrl("src");

                System.out.println("¡Encontrada una imagen!");
                System.out.println("Su url es: " + src);

                getImages(src);

            }

        } catch (IOException ex) {
            System.err.println("Error");
        }

    }

    private static void getImages(String src) throws IOException {

        String folder = null;

        //Extrae el nombre de la imagen
        int indexname = src.lastIndexOf("/");

        if (indexname == src.length()) {
            src = src.substring(1, indexname);
        }

        indexname = src.lastIndexOf("/");
        String name = src.substring(indexname, src.length());

        System.out.println("El nombre del archivo es: " + name);
        System.out.println("");
    }

    static void createImage(URL url) throws IOException {
        BufferedImage image = null;
        try {

            image = ImageIO.read(url);

            ImageIO.write(image, "png", new File("C:\\Users\\Jordi\\Desktop\\lib"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }

}

/*//Open a URL Stream
 URL url = new URL(src);
 InputStream in = url.openStream();

 OutputStream out = new BufferedOutputStream(new FileOutputStream(folderPath + name));

 for (int b; (b = in.read()) != -1;) {
 out.write(b);
 }

 out.close();
 in.close();*/
/*static void createImage(URL url) throws IOException {

 BufferedImage image = null;
 try {

 image = ImageIO.read(url);

 ImageIO.write(image, "png", new File("out.png"));

 } catch (IOException e) {
 e.printStackTrace();
 }
 System.out.println("Done");

 }*/

/*static void testIPs() {
 InetAddress[] addresses = new InetAddress[2];

 try {
 addresses[0] = InetAddress.getLoopbackAddress();
 addresses[1] = InetAddress.getByName("jordima.blogspot.es");

 for (InetAddress adress : addresses) {
 System.out.println("La IP és " + adress.getHostAddress());
 if (adress.isLoopbackAddress()) {
 System.out.println(adress.getHostName() + " té una adreça loopback");
 } else {
 System.out.println(adress.getHostName() + " no té una adreça loopback");
 }
 }

 } catch (UnknownHostException e) {
 System.out.println(e.getMessage());

 }
 }*/
