/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica9;

import java.util.Stack;

/**
 *
 * @author Jordi
 */
public class Hilos extends Thread {

    int arraynumbers[];

    public Hilos(int arraynumbers[]) {
        this.arraynumbers = arraynumbers;
    }

    public void run() {
        while (ArrayYsynchronized.nextNumberToProcess < arraynumbers.length) {
            Integer number = arraynumbers[ArrayYsynchronized.nextNumberToProcess];
            // procesarNumero(number);
            try {
                Thread.sleep(8);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hemos procesado numero " + number);
            ArrayYsynchronized.syncindex();
        }
    }
}
