/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica9;

import java.util.Date;
import java.util.Stack;

/**
 *
 * @author Jordi
 *
 */
public class ArrayYsynchronized {

    static int[] numeros = new int[2000];
    static int nextNumberToProcess = 0;
    static long tiempoInicial = new Date().getTime();

    public static void main(String[] args) {

        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = i;
        }

        Hilos miHilo1 = new Hilos(numeros);
        Hilos miHilo2 = new Hilos(numeros);
        Hilos miHilo3 = new Hilos(numeros);
        Hilos miHilo4 = new Hilos(numeros);
        Hilos miHilo5 = new Hilos(numeros);

        miHilo1.start();
        miHilo2.start();
        miHilo3.start();
        miHilo4.start();
        miHilo5.start();

        try {
            miHilo1.join();
            miHilo2.join();
            miHilo3.join();
            miHilo4.join();
            miHilo5.join();
        } catch (Exception e) {
            System.out.println("Error en los hilos");
        }

        System.out.println("Ha tardado " + (new Date().getTime()
                - tiempoInicial) / 1000 + " segundos");
    }

    public static synchronized int syncindex() {
        return nextNumberToProcess++;
    }

}
