/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public class Punt implements Figura {

    private int punto1, punto2;

    public Punt(int punto1, int punto2) {
        this.punto1 = punto1;
        this.punto2 = punto2;
    }

    @Override
    public void pintar(Graphics g) {
        g.drawLine(punto1, punto2, punto1, punto2);
    }

}
