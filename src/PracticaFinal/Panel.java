/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Jordi
 */
public class Panel extends JPanel {

    private Figura figura;

    //Declarar variable de tipo Rellenable
    //Declarar variable "esRellenabe"
    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public void paint(Graphics g) {
        removeAll();
        repaint();
        revalidate();

        if (figura != null) {

            //Añadir IF según variable "esRellenable".
            figura.pintar(g);
        }
    }

    public void setFigura(Figura figura) {
        this.figura = figura;

    }

    //Implementar método setRellable, que ademas de asignar el parámetro rellenable, también asignará la variable esRellenable.
}
