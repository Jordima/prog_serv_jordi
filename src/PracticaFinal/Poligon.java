/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public class Poligon implements Figura, Rellenable {

    private int punto1poli;
    private int[] vecx;
    private int[] vecy;

    private Color col;

    public Poligon(int[] vecx, int[] vecy, int punto1poli) {
        this.punto1poli = punto1poli;
        this.vecx = vecx;
        this.vecy = vecy;

    }

    @Override
    public void pintar(Graphics graphics) {
        graphics.drawPolygon(vecx, vecy, punto1poli);

    }

    @Override
    public void ponerColor(Color color) {
        this.col = color;
    }

    @Override
    public void rellenoPintar(Graphics graphics) {
        graphics.fillPolygon(vecx, vecy, punto1poli);
    }

}
