package PracticaFinal;

import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import javax.swing.JFrame;

public class Rectangle implements Figura, Rellenable {

    private int anchorect, altorect, punto1rect, punto2rect;

    private Color col;

    public Rectangle(int anchorect, int altorect, int punto1rect, int punto2rect) {
        this.anchorect = anchorect;
        this.altorect = altorect;
        this.punto1rect = punto1rect;
        this.punto2rect = punto2rect;
    }

    @Override
    public void pintar(Graphics graphics) {
        graphics.drawRect(anchorect, altorect, punto1rect, punto2rect);
    }

    @Override
    public void ponerColor(Color color) {
        this.col = color;
    }

    @Override
    public void rellenoPintar(Graphics graphics) {
        graphics.fillRect(altorect, altorect, altorect, altorect);
    }
}
