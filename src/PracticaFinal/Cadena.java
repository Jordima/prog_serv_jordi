/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Graphics;
import java.text.AttributedCharacterIterator;

/**
 *
 * @author Jordi
 */
public class Cadena implements Figura {

   private int punt1, punt2;

    private String frase;

    Cadena(String frase, int punt1, int punt2) {
        this.punt1 = punt1;
        this.punt2 = punt2;

        this.frase = frase;
    }

    @Override
    public void pintar(Graphics graphics) {
        graphics.drawString(frase, punt1, punt2);
    }
}
