/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public class Cercle implements Figura, Rellenable {

    private int punto1cerc, punto2cerc, punto3cerc;

    private Color col;

    public Cercle(int punto1cerc, int punto2cerc, int punto3cerc) {
        this.punto1cerc = punto1cerc;
        this.punto2cerc = punto2cerc;
        this.punto3cerc = punto3cerc;
    }

    @Override
    public void pintar(Graphics graphics) {
        graphics.drawOval(punto1cerc, punto2cerc, punto3cerc * 2, punto3cerc * 2);
    }

    @Override
    public void ponerColor(Color color) {
        this.col = color;
    }

    @Override
    public void rellenoPintar(Graphics graphics) {
        graphics.fillOval(punto1cerc, punto2cerc, punto3cerc * 2, punto3cerc * 2);
    }

}
