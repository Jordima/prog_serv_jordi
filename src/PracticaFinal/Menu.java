/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Los saltos de linea con \n es una contrabarra!!!
package PracticaFinal;

import java.util.Scanner;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.lang.String;

/**
 *
 * @author Jordi
 */
public class Menu {

    //Recogemos el texto por consola con Scanner
    static Scanner scanner = new Scanner(System.in);
    //variable para la opcion que elige el usuario
    static int opcion = -1;
    //static int num1 = 0, num2 = 0;

    //variable arrays para los vectores del poligono
    public static int[] vecx;
    public static int[] vecy;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        final Panel panel = new Panel();

        Runnable guiThread = (new Runnable() {
            @Override
            //runframe
            public void run() {
                JFrame frame;

                //Create and set up the window.
                frame = new JFrame("PanelPractica");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                frame.setBounds(100, 100, 800, 600);
                frame.getContentPane().add(panel);

                //Mostramos el frame.
                frame.setVisible(true);
            }
        });
        try {
            SwingUtilities.invokeAndWait(guiThread);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        //Mientras sea 0, vamos a seguir preguntando
        while (opcion != 0) {
            //El try catch de siempre para evitar un errror
            try {
                System.out.println(
                        "Benvingut, escriu la lletra corresponent per escollir l'opció que vols: \n"
                        + "1.Cadena de text\n"
                        + "2.Punt\n"
                        + "3.Linia\n"
                        + "4.Cercle\n"
                        + "5.Quadrat\n"
                        + "6.Rectangle\n"
                        + "7.Poligon\n"
                        + "8.Dibuixar historic de figures\n"
                        + "0.Sortir");
                //Con el scanner nextline recogemos lavariable que se nos pasa
                opcion = Integer.parseInt(scanner.nextLine());

                //Switch de toda la vida
                switch (opcion) {
                    case 1:
                        panel.setVisible(false);

                        System.out.println("Introduce la posicion de la frase");
                        Scanner lectorfrase;
                        lectorfrase = new Scanner(System.in);
                        System.out.print("X -> ");
                        int punt1 = lectorfrase.nextInt();
                        System.out.println("Y -> ");
                        int punt2 = lectorfrase.nextInt();
                        System.out.println("¿Que quieres escribir? ");
                        //Thats because the Scanner#nextInt method does not consume 
                        //the last newline character of your input, and thus that newline is consumed in the next call to Scanner#nextLine
                        lectorfrase.nextLine();
                        String frase = lectorfrase.nextLine();

                        panel.setFigura(new Cadena(frase, punt1, punt2));
                        panel.setVisible(true);
                        panel.repaint();
                        break;

                    case 2:
                        panel.setVisible(false);

                        //Sale el pixel muy oequeño, pero sale, probar con posicion 1,1 para verlo claro)
                        System.out.println("Introdueix la posicio del punt");
                        Scanner lectorpunt;
                        lectorpunt = new Scanner(System.in);
                        System.out.print("X -> ");
                        int punto1 = lectorpunt.nextInt();
                        System.out.println("Y -> ");
                        int punto2 = lectorpunt.nextInt();

                        panel.setFigura(new Punt(punto1, punto2));
                        panel.setVisible(true);
                        panel.repaint();
                        break;

                    case 3:
                        panel.setVisible(false);

                        System.out.println("Introdueix les dimensions i posició de la linea");
                        Scanner lectorlinea;
                        lectorlinea = new Scanner(System.in);
                        System.out.print("Ancho -> ");
                        int ancholinea = lectorlinea.nextInt();
                        System.out.println("Alto -> ");
                        int altolinea = lectorlinea.nextInt();
                        System.out.print("X -> ");
                        int punto1linea = lectorlinea.nextInt();
                        System.out.println("Y -> ");
                        int punto2linea = lectorlinea.nextInt();

                        panel.setFigura(new Linea(ancholinea, altolinea, punto1linea, punto2linea));
                        panel.setVisible(true);
                        panel.repaint();
                        break;

                    case 4:
                        panel.setVisible(false);

                        System.out.println("Introdueix els punts per crear un cercle");
                        Scanner lectorcercle;
                        lectorcercle = new Scanner(System.in);
                        System.out.print("X -> ");
                        int punto1cerc = lectorcercle.nextInt();
                        System.out.println("Y -> ");
                        int punto2cerc = lectorcercle.nextInt();
                        System.out.println("Radio -> ");
                        int punto3cerc = lectorcercle.nextInt();

                        //Pedir si se desea rellenable.
                        //System.out.println("Rellena? ");
                        //Añadir IF si desea relleno, entonces no llamar a setFigura, si no llamar a setRellenable
                        panel.setFigura(new Cercle(punto1cerc, punto2cerc, punto3cerc));
                        panel.setVisible(true);
                        panel.repaint();// ESTO LLAMA A PAINT DE MI CLASE PANEL?
                        break;
                    case 5:
                        panel.setVisible(false);

                        System.out.println("Introdueix les dimensions i posició del quadrat");
                        Scanner lectorquadrat;
                        lectorquadrat = new Scanner(System.in);
                        System.out.print("Ancho -> ");
                        int anchoquadre = lectorquadrat.nextInt();
                        System.out.println("Alto -> ");
                        int altoquadre = lectorquadrat.nextInt();
                        System.out.print("X -> ");
                        int punto1quadre = lectorquadrat.nextInt();

                        panel.setFigura(new Quadrat(punto1quadre, punto1quadre, anchoquadre, altoquadre));
                        panel.setVisible(true);
                        panel.repaint();
                        break;
                    case 6:
                        panel.setVisible(false);

                        System.out.println("Introdueix els cuatre punts per crear un rectangle");
                        Scanner lectorrectangle;
                        lectorrectangle = new Scanner(System.in);
                        System.out.print("Ancho -> ");
                        int anchorect = lectorrectangle.nextInt();
                        System.out.println("Alto -> ");
                        int altorect = lectorrectangle.nextInt();
                        System.out.print("X -> ");
                        int punto1rect = lectorrectangle.nextInt();
                        System.out.println("Y -> ");
                        int punto2rect = lectorrectangle.nextInt();

                        panel.setFigura(new Rectangle(punto1rect, punto2rect, anchorect, altorect));
                        panel.setVisible(true);
                        panel.repaint();

                        break;
                    case 7:
                        //con estas dimensiones sale, por ejemplo.
                        /*   int xpoints[] = {25, 145, 25, 145, 25};
                         int ypoints[] = {25, 25, 145, 145, 25};
                         */
                        panel.setVisible(false);

                        System.out.println("Introdueix les dades per crear un poligon");
                        Scanner lectorpoligon;
                        lectorpoligon = new Scanner(System.in);
                        System.out.print("Lados -> ");
                        int punto1poli = lectorpoligon.nextInt();
                        vecx = new int[punto1poli];
                        vecy = new int[punto1poli];

                        for (int i = 0; i < punto1poli; i++) {
                            System.out.println("Introdueix coordenada X");
                            vecx[i] = lectorpoligon.nextInt();
                            System.out.println("Introdueix coordenada Y");
                            vecy[i] = lectorpoligon.nextInt();
                        }

                        panel.setFigura(new Poligon(vecx, vecy, punto1poli));
                        panel.setVisible(true);
                        panel.repaint();

                        break;
                    case 8:
                        panel.setVisible(true);
                        break;
                    case 0:
                        System.out.println("¡Fins aviat!");
                        break;
                    default:
                        System.out.println("No has introduit un numero de les opcions");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Error, torna a escriure.");
            }
        }

    }
}
