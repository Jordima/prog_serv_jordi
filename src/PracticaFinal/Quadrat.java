/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public class Quadrat implements Figura, Rellenable {

    private int anchoquadre, altoquadre, punto1quadre, punto2quadre;

    private Color col;

    public Quadrat(int anchoquadre, int altoquadre, int punto1quadre, int punto2quadre) {

        this.anchoquadre = anchoquadre;
        this.altoquadre = altoquadre;
        this.punto1quadre = punto1quadre;
        //this.punto2quadre = punto2quadre;
    }

    @Override
    public void pintar(Graphics graphics) {
        graphics.drawRect(anchoquadre, altoquadre, punto1quadre, punto1quadre);
    }

    @Override
    public void ponerColor(Color color) {
        this.col = color;
    }

    @Override
    public void rellenoPintar(Graphics graphics) {
        graphics.fillRect(altoquadre, altoquadre, altoquadre, altoquadre);
    }
}
