/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public interface Rellenable extends Figura {

    public boolean isRellenable = false;

    void ponerColor(Color color);

    void rellenoPintar(Graphics graphics);

}
