/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaFinal;

import java.awt.Graphics;

/**
 *
 * @author Jordi
 */
public class Linea implements Figura {

    private int ancholinea, altolinea, punto1linea, punto2linea;
    
    public Linea(int ancholinea, int altolinea, int punto1linea, int punto2linea) {

        this.ancholinea = ancholinea;
        this.altolinea = altolinea;
        this.punto1linea = punto1linea;
        this.punto2linea = punto2linea;
    }
    
    @Override
    public void pintar(Graphics graphics) {
        graphics.drawLine(ancholinea,altolinea, punto1linea, punto2linea);
    }

    /*public Linea(int x, int y) {
        super(x, y);
    }
    @Override
    public void paint(Graphics g) {
        
        g.drawLine(30, 35, x, y);

    }*/
}
