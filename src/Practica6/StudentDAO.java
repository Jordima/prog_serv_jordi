/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica6;

/**
 *
 * @author Jordi
 */
public interface StudentDAO {

    Student getStudent(Student stud);

    void removeStudent(Student stud);

    void addStudent(Student stud);

    void contractStudent(Student stud);

}
