/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_diario;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Agendas {
    int anno;
    int numPagina;
    LinkedList<Pagina> paginas = new LinkedList<>();
    
    public Agendas(int anno, int numPagina){
        this.anno = 2016;
        this.numPagina = numPagina;
    }
    
    public Agendas(){};
    
    public void pasarPaginaAdelante(){
        numPagina++;
        
        System.out.println("Estas en la pagina: " + numPagina);
        
        AccionesAgendas();
    }
    
    public void retrocederPagina(){
        numPagina--;
        
        System.out.println("Estas en la pagina: " + numPagina);
        
        AccionesAgendas();
    }
    
    public int AccionesAgendas(){
       try {
                int choice = 0;
                    
                System.out.println("......................");
                System.out.println("AGENDA");
                System.out.println("......................");
                System.out.println("1-Pasar Pagina");
                System.out.println("2-Retrocedes Pagina");
                System.out.println("3-Insertar Cita");
                System.out.println("4-Vacias Agenda");
                System.out.println("5-Salir");
                System.out.println("......................");
                
                Scanner sc = new Scanner (System.in);
                choice = sc.nextInt();

        switch (choice){
            case 1:
             pasarPaginaAdelante();
             break;
                
            case 2:
             retrocederPagina();
             break;
                
            case 3:
             Pagina p = new Pagina();
             p.insertarCita(this);
             break;
                
            case 4:
             Pagina p1 = new Pagina();
             p1.vaciarAgenda(this);
             break;
               
            case 5:
             System.out.println("Adios");
             break;
        }
        
        } catch (Exception e){
            e.printStackTrace();
            
            return -1;
        }
        return 0;
       
         }
}
