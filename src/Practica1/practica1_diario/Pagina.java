/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_diario;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Pagina {
    int dia;
    int mes;
    boolean isFestivo;
    LinkedList<cita> citas = new LinkedList<>();
    
    public Pagina(int dia, int mes, LinkedList<cita> citas){
        this.dia = dia;
        this.mes = mes;
        this.isFestivo = true;
        this.citas = citas;
    }
    
    public Pagina(){};
    
    public void insertarCita(Agendas a){
        boolean correcto = false;
        String meses = "";
        int pagina = 0;
        do{
            try{
                Scanner sc = new Scanner (System.in);
                int mesCita;
                System.out.println("¿Para que mes quieres la cita?");
                mesCita = sc.nextInt();
              
          
                
                if((mesCita >=1) && (mesCita <= 12)){
                   
                   switch(mesCita){
                       case 1:
                         if(mesCita == 1){
                             correcto = true;
                             meses = "Enero";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 2:
                         if(mesCita == 2){
                             correcto = true;
                             meses = "Febrero";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 3:
                         if(mesCita == 3){
                             correcto = true;
                             meses = "Marzo";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 4:
                         if(mesCita == 4){
                             correcto = true;
                             meses = "Abríl";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 5:
                         if(mesCita == 5){
                             correcto = true;
                             meses = "Mayo";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 6:
                         if(mesCita == 6){
                             correcto = true;
                             meses = "Junio";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 7:
                         if(mesCita == 7){
                             correcto = true;
                             meses = "Julio";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 8:
                         if(mesCita == 8){
                             correcto = true;
                             meses = "Agosto";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 9:
                         if(mesCita == 9){
                             correcto = true;
                             meses = "Septiembre";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 10:
                         if(mesCita == 10){
                             correcto = true;
                             meses = "Octubre";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 11:
                         if(mesCita == 11){
                             correcto = true;
                             meses = "Noviembre";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;

                       case 12:
                         if(mesCita == 12){
                             correcto = true;
                             meses = "Diciembre";
                         }else{
                             System.out.println("Lo siento, los datos introducidos no son correctos");
                         }
                         break;
                   }
                }else{
                   System.out.println("Lo siento, los datos introducidos no son correctos"); 
                }
          
            }catch(Exception e){
                e.printStackTrace();
            }
        }while(correcto == false);
        
        System.out.println("Has introducido una cita para el dia "+ dia + " de "+ meses);
        
        cita c1 = new cita();
        
        c1.crearCita();
        
        citas.add(c1);
        
        Pagina p = new Pagina(dia, mes, citas);
        
        a.paginas.add(p);
        
        a.AccionesAgendas();

    }
    
    public void vaciarAgenda(Agendas a){
        
        if(a.paginas.isEmpty() == false){
            
            a.paginas.clear();
            System.out.println("Has limpiado toda la agenda");
        
        }else {
            System.out.println("La agenda no tiene citas");
        }
        
        a.AccionesAgendas();
    }
    
}