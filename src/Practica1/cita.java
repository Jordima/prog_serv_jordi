/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica1;

/**
 *
 * @author alumno
 */
public class cita {

    int horaInicio;
    int horaFinal;
    String contenido;

    public cita(int horaInicio, int horaFinal, String contenido) {
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.contenido = contenido;
    }

    public cita() {
    }

    ;
    
    public cita crearCita() {
        boolean correcto = false;

        do {
            try {
                String inicio = "Introduce la hora de inicio";
                String Hfinal = "Introduce la hora de finalizacion";

                horaInicio = Integer.parseInt(inicio);
                horaFinal = Integer.parseInt(Hfinal);
                contenido = "Introduce el contenido";

                correcto = true;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Lo siento, los datos introducidos no son correctos");
            }
        } while (correcto == false);

        cita c = new cita(horaInicio, horaFinal, contenido);

        return c;
    }
}
