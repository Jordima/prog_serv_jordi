/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica1;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Principal {

    int año;
    int numPagina;
    LinkedList<Pagina> paginas = new LinkedList<>();

    public Principal(int año, int numPagina) {
        this.año = 2016;
        this.numPagina = numPagina;
    }

    public Principal(){};
    
    public void pasarPaginaAlante() {
        numPagina++;

        System.out.println("Te encuentras en la pagina numero: " + numPagina);

        Accion();
    }

    public void retrocederPagina() {
        numPagina--;

        System.out.println("Ahora estas en la pagina: " + numPagina);

        Accion();
    }

    public void Accion() {
        try {
            int choice = 0;

            System.out.println("¡Bienvenido a la agenda digital!");
            System.out.println("Teclee el numero que desee que corresponda a la opcion  ((si no funciona o peta, lo siento)) ");
            System.out.println("1-Pasa Pagina, de la agenda");
            System.out.println("2-Pagina atras");
            System.out.println("3-Inserte una cita, a la agenda");
            System.out.println("4-Vaciar registros");
            System.out.println("5-Sal");

            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();
            System.out.println(choice);

            switch (choice) {
                case 1:
                    pasarPaginaAlante();
                    break;

                case 2:
                    retrocederPagina();
                    break;

                case 3:
                    Pagina p = new Pagina();
                    p.insertarCita(this);
                    break;

                case 4:
                    Pagina p1 = new Pagina();
                    p1.vaciarAgenda(this);
                    break;

                case 5:
                    System.out.println("Pimienta. "
                            + "Hasta luego!");
                    break;
            }
        } catch (Exception e) {

        }

    }
}
