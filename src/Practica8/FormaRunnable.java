/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica8;
/**
 *
 * @author Jordi
 */
import java.util.Date;
import java.util.Stack;

public class FormaRunnable implements Runnable {

    Stack<Integer> numbers = new Stack<Integer>();

    public FormaRunnable(Stack<Integer> numbers) {
        this.numbers = numbers;
    }

    public static void main(String[] args) {

        Stack<Integer> numbers = new Stack<Integer>();
        //Rellenamos pila
        for (int i = 0; i < 1000; i++) {
            numbers.push(i);
        }
        long tiempoInicial = new Date().getTime();

        Runnable run1 = new FormaRunnable(numbers);
        Runnable run2 = new FormaRunnable(numbers);
        Runnable run3 = new FormaRunnable(numbers);
        Runnable run4 = new FormaRunnable(numbers);
        Runnable run5 = new FormaRunnable(numbers);

        new Thread(run1).start();
        new Thread(run2).start();
        new Thread(run3).start();
        new Thread(run4).start();
        new Thread(run5).start();

        System.out.println(
                "Ha tardado " + (new Date().getTime()
                - tiempoInicial) / 1000 + " segundos");
    }

    public void run() {
        while (!numbers.isEmpty()) {
            Integer number = numbers.pop();
            // procesarNumero(number);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hemos procesado numero " + number);
        }
    }
}
