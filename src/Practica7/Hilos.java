/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica7;

import java.util.Stack;

/**
 *
 * @author Jordi
 */
public class Hilos extends Thread {

    Stack<Integer> numbers;

    public Hilos(Stack<Integer> numbers) {
        this.numbers = numbers;
    }

    public void run() {
        while (!numbers.isEmpty()) {
            Integer number = numbers.pop();
            // procesarNumero(number);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hemos procesado numero " + number);
        }
    }
}
