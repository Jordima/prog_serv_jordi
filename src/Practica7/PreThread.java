/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica7;

import java.util.Date;
import java.util.Stack;

/**
 * @author Xavi Torrens
 */
// Es una chorrada de hacer sugun xavi. No es mas alla de añadir un metodo
// hay que utilizar una pila con 5 hilos
public class PreThread {

    public static void main(String[] args) {
        Stack<Integer> numbers = new Stack<Integer>();
        //Rellenamos pila
        for (int i = 0; i < 1000; i++) {
            numbers.push(i);
        }
        long tiempoInicial = new Date().getTime();

        Hilos miHilo1 = new Hilos(numbers);
        Hilos miHilo2 = new Hilos(numbers);
        Hilos miHilo3 = new Hilos(numbers);
        Hilos miHilo4 = new Hilos(numbers);
        Hilos miHilo5 = new Hilos(numbers);

        miHilo1.start();
        miHilo2.start();
        miHilo3.start();
        miHilo4.start();
        miHilo5.start();

        System.out.println("Ha tardado " + (new Date().getTime()
                - tiempoInicial) / 1000 + " segundos");
    }

}
