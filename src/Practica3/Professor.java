/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3;

/**
 *
 * @author alumno
 */
public class Professor extends PersonalGestio {

    String assignatura;

    public Professor(
            int nie,
            String nombre,
            String direccion,
            String email,
            String departamento,
            String puesto,
            int salario,
            String asignatura) {
        super(nie, nombre, direccion, email, salario);
        this.assignatura = assignatura;
    }

}
